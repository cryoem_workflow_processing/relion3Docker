# Docker container for RELION 3.0

Run:
```
docker run -it --user $(id -u):$(id -g) --gpus all -v `pwd`:/work sbobkov/relion3 relion_executable
```

`relion_executable` - relion executable command with paramenters 